export interface Player {
  _id: string;
  idSocket: string;
  username: string;
}
