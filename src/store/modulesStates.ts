import {Player} from '@/models/player';

export interface LoadingModuleState {
  nbLoad: number;
}
export interface PlayerModuleState {
  _players: Player[];
}

export type RootState = LoadingModuleState & PlayerModuleState;
