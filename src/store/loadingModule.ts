import {VuexModule, Module, Mutation} from 'vuex-module-decorators';
import store from '.';

@Module({dynamic: true, store, name: 'loading'})
class LoadingModule extends VuexModule {
  public nbLoad = 0;

  get isLoading() {
    return this.nbLoad !== 0;
  }

  @Mutation
  public addLoading() {
    this.nbLoad += 1;
  }

  @Mutation
  removeLoading() {
    this.nbLoad -= 1;
    if (this.nbLoad < 0) {
      this.nbLoad = 0;
    }
  }
}

export default LoadingModule;
