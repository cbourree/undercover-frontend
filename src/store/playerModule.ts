import axios from 'axios';
import {Player} from '@/models/player';
import {VuexModule, Module, Mutation, Action} from 'vuex-module-decorators';
import store from '.';

@Module({dynamic: true, store, name: 'player'})
class PlayerModule extends VuexModule {
  private _players: Player[] = [];

  private _playersGame: Player[] = [];

  private _playersGameLeft: Player[] = [];

  get players() {
    return this._players ?? [];
  }

  get playersGame() {
    return this._playersGame ?? [];
  }

  get playersGameLeft() {
    return this._playersGameLeft ?? [];
  }

  @Mutation
  public setPlayers(players: Player[]) {
    this._players = players;
  }

  @Mutation
  public setPlayersGameLeft(players: Player[]) {
    this._playersGameLeft = players;
  }

  @Mutation
  public setPlayersGame(players: Player[]) {
    this._playersGame = players;
  }

  @Mutation
  public addPlayerGame(players: Player) {
    this._playersGame.push(players);
  }

  @Action({rawError: true})
  public getPlayers(): Promise<Player[]> {
    this.context.commit('addLoading');
    return axios
      .get<Player[]>('https://undercover.bourree.fr/api/players')
      .then(result => {
        this.context.commit('setPlayers', result.data);
        return result.data;
      })
      .finally(() => {
        this.context.commit('removeLoading');
      });
  }

  @Action({rawError: true})
  public getPlayersFromGame(idgame: string): Promise<Player[]> {
    this.context.commit('addLoading');
    return axios
      .get<Player[]>(`https://undercover.bourree.fr/api/games/${idgame}/players`)
      .then(result => {
        this.context.commit('setPlayersGame', result.data);
        this.context.commit('setPlayersGameLeft', result.data);
        return result.data;
      })
      .finally(() => {
        this.context.commit('removeLoading');
      });
  }

  @Action({rawError: true})
  public initPlayersGame(players: Player[]): void {
    this.context.commit('setPlayersGame', players);
    this.context.commit('setPlayersGameLeft', players);
  }
}

export default PlayerModule;
