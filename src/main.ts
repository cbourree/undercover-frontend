import Vue from 'vue';
import VueSocketIOExt from 'vue-socket.io-extended';
import io from 'socket.io-client';
import VueClipboard from 'vue-clipboard2';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

const socket = io('https://undercover.bourree.fr/');

Vue.use(VueClipboard);
Vue.use(VueSocketIOExt, socket);
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');
