export default class LogService {
  static log(...args: any): void {
    // eslint-disable-next-line
    console.log(...args);
  }

  static info(...args: any): void {
    // eslint-disable-next-line
    console.info(...args);
  }

  static debug(...args: any): void {
    // eslint-disable-next-line
    console.debug(...args);
  }

  static error(...args: any): void {
    // eslint-disable-next-line
    console.error(...args);
  }
}
